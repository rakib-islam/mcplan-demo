package edu.oregonstate.eecs.mcplan.domains.freecell;

import edu.oregonstate.eecs.mcplan.Agent;
import edu.oregonstate.eecs.mcplan.Simulator;
import edu.oregonstate.eecs.mcplan.State;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mislam1 on 2/1/16.
 */
public class FreeCellAgent extends Agent {

    private static int NUMBER_OF_ACTIONS = 5;
    public FreeCellAgent(){
        name_ = "FreeCellAgent";
    }


    /* todo:this will be our base policy
    *   1) a card to a home cell, (1)
        2) a king to an empty column,
        3) a card from a column, preferring a move that will either expose a
        card that can be moved to a home cell or empty a column in the
        fewest moves,
        4) a card to an empty column provided that another card is ready to
        move on top
        5) a card to a column with the reverse of test #3 (in other words, a card
        to a column that does not have any cards that are ready to move to
        their home cells nearly free and is not almost empty),
        6) a card off a free cell,
        7) a card that is the last one on a column,
        8) a card to any other column,
        9) a card to a free cell, and finally,
        10) a card to an empty column.

        1. freecell to foundation
        2. column to foundation
        3. column to column (select the action which will transfer most cards at one action)
        4. column to freecell
        5. freecell to column
    * */
    /*todo: 1. detect the loop of actions
      todo: 2. we can set the horizon size to 150 (in general we can say that max number of moves needed is 150)
      todo: 3.
    * */

    @Override
    public <S extends State, A> A selectAction(S state, Simulator<S, A> iSimulator) {
        iSimulator.setState(state);
        List<A> actions = iSimulator.getLegalActions();
        A actionFromPrior = (A) selectActionFromPriorKnowledge((List<FreeCellAction>)actions, (FreeCellNode)iSimulator.getState());
        if (actionFromPrior != null) {
            return actionFromPrior;
        }
        else {
            return actions.get((int) (Math.random() * actions.size()));
        }
        /*Collections.sort(actions, new Comparator<A>() {
            @Override
            public int compare(A o1, A o2) {
                FreeCellAction a1 = (FreeCellAction) o1;
                FreeCellAction a2 = (FreeCellAction) o2;
                Integer a1Id = a1.getActionTypeId();
                Integer a2Id = a2.getActionTypeId();
                return a1Id.compareTo(a2Id);
            }
        });*/
        /*List <FreeCellAction> freeCellActionList = new ArrayList<>();
        for (A action: actions) freeCellActionList.add((FreeCellAction) action);
        if(freeCellActionList.get(0).getActionTypeId() == 1 || freeCellActionList.get(0).getActionTypeId() == 2 ) return (A) freeCellActionList.get(0);
        else {
            int selectedAction = (int) (Math.random() * freeCellActionList.size());
            //System.out.println("Rand Selected Action: " + selectedAction + " out of " + actions.size());
            return actions.get(selectedAction);
        }*/

        /*
        List <List<FreeCellAction>> freeCellActionList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ACTIONS; ++i) { freeCellActionList.add(i, new ArrayList<FreeCellAction>());}
        for (A action: actions) {
            FreeCellAction freeCellAction = (FreeCellAction) action;
            //System.out.println(freeCellAction + " " + freeCellAction.getActionTypeId());
            int currentActionTypeId = freeCellAction.getActionTypeId();
            if (currentActionTypeId == 1 || currentActionTypeId ==2) {
                return (A) freeCellAction;
            }
            else freeCellActionList.get(0).add(freeCellAction);
        }
        if (actions.size() >= 1) {
            int maxScore = -1;
            int maxActionId = -1;
            int freeCellActionListSize = freeCellActionList.size();
            for (int i = 0; i < freeCellActionListSize; ++i) {
                int currentListSize = freeCellActionList.get(i).size();
                for (int j = 0; j < currentListSize; ++j) {
                    if ( currentListSize > 0) {
                        //int scoreOfFinalState = Arbiter.scoringFunction.eval(freeCellActionList.get(i).get(j));
                        return (A)freeCellActionList.get(i).get((int) (Math.random() * freeCellActionList.get(i).size()));
                    }

                }
                /*int currentListSize = freeCellActionList.get(i).size();
                if ( currentListSize >= 1) {
                    if ( i == 2) {
                        int maxCardCount = -1;
                        int maxCardActionIndex = -1;
                        List <FreeCellAction> columnToColumnActionList = freeCellActionList.get(i);
                        for (int j = 0; j < currentListSize; ++j) {
                            if (columnToColumnActionList.get(j).cardCount() > maxCardCount) { maxCardActionIndex = j; maxCardCount=columnToColumnActionList.get(j).cardCount(); }
                        }
                        return (A)freeCellActionList.get(i).get(maxCardActionIndex);
                    }
                    else {
                        int randSelected = (int) (Math.random() * currentListSize);
                        //System.out.println("random action: " + randSelected);
                        return (A)freeCellActionList.get(i).get((int) randSelected);
                    }

                }
            }

        }*/
        //return null;
        //return actions.get((int) (Math.random() * actions.size()));
    }

    @Override
    public <S extends State, A> List<A> selectActions(S state, Simulator<S, A> iSimulator) {
        return null;
    }

    private FreeCellAction selectActionFromPriorKnowledge(List<FreeCellAction> legalActions, FreeCellNode fcn) {
        String ranks = ".A23456789TJQK";
        String suits = "CDHS";
        int rankLow = 100;
        int cardDepth  = -1;
        List<Integer> foundationtarget = new ArrayList<>();
        short[] foundation  =fcn.getFoundations();
        for (int i = 0; i < 4; ++i) {
            foundationtarget.add((int) foundation[i]);
        }
        //if we have 1 action no choice for this
        if (legalActions.size() == 1)
            return legalActions.get(0);
        else if (legalActions.size() > 0) {
            FreeCellAction bestAction = null;
            int bestActionIndex = 10;
            for (FreeCellAction fca : legalActions) {

                //1. if one move to foundation available then move it.
                if (fca.actionTypeId == 1) {
                    bestAction= fca;
                    bestActionIndex = 1;
                }
                //2. move from freecell to foundation
                else if (fca.actionTypeId == 2) {
                    if (bestActionIndex > 2 ) {
                        bestAction = fca;
                        bestActionIndex = 2;
                    }
                }
                //3. move column to column
                else if (fca.actionTypeId == 3 && ranks.charAt(fca.cardRank()) == 'K' ) {
                    //move a king to an empty column
                    if (fcn.getCols()[fca.cardDestination()].rank() == 0) {
                        if (bestActionIndex > 3 ) {
                            bestAction = fca;
                            bestActionIndex = 3;
                        }
                    }
                    //remove a card that expose a low ranked card from bottom
                    //remove a card which has a foundation movable card
                    //System.out.println(fca.cardRank() + " " + fca.cardSuit() + " " + fca.cardSource() + " " + fca.cardDestination());
                    //return fca;
                }

                /*else if (fca.actionTypeId == 3) {
                    Column fromCol =  fcn.getCols()[fca.cardSource()];
                    Column toCol =  fcn.getCols()[fca.cardDestination()];
                    short[] fromColCards = fromCol.cards;
                    for (int i = 0; i < fromColCards.length; ++i) {
                        int card = fromColCards[i];
                        if (card > 0) {
                            int suit = (short) (((card - 1) % 4));       // subtract 1 since '0' is invalid card.
                            int rank = (short) (1 + ((card - 1) >> 2));  // rank extracted this way.
                            if (foundationtarget.get(suit) + 1 == rank) {
                                if (bestActionIndex >= 3 && rank < rankLow) {
                                    bestAction = fca;
                                    bestActionIndex = 3;
                                    rankLow = rank;
                                }

                            }
                        }
                    }

                    short[] cardsMovePossible = fca.cardCount();
                    int count = 0;
                    for (int i = 0; i< cardsMovePossible.length; ++i) {
                        if (cardsMovePossible[i] > 0) { count++;}
                    }
                    if (count >=3 ) {
                        bestAction = fca;
                        bestActionIndex = 3;
                        return bestAction;
                    }

                }*/
                //4.	Move cards off from FreeCell to columns.
                /*else if (fca.actionTypeId == 4) {

                    if (fcn.getCols()[fca.cardDestination()].rank() == 0 &&
                            ranks.charAt(fca.cardRank()) == 'K') {
                        return fca;
                    }
                    if (bestActionIndex > 4 ) {
                        bestAction = fca;
                        bestActionIndex = 4;
                    }
                }*/
                //3.rest of the column moves
                //Move a card that is the last one in the column
                /*else if (fca.actionTypeId == 3 && fcn.getCols()[fca.cardSource()].num == 1) {
                    if (bestActionIndex > 5 ) {
                        bestAction = fca;
                        bestActionIndex = 5;
                    }
                }*/
                //5. move from column to freecell
                /*else if (fca.actionTypeId == 5) {
                    Column fromCol =  fcn.getCols()[fca.cardSource()];
                    short[] fromColCards = fromCol.cards;
                    for (int i = 0; i < fromColCards.length; ++i) {
                        int card = fromColCards[i];
                        if (card > 0) {
                            int suit = (short) (((card - 1) % 4));       // subtract 1 since '0' is invalid card.
                            int rank = (short) (1 + ((card - 1) >> 2));  // rank extracted this way.
                            if (foundationtarget.get(suit) + 1 == rank) {
                                if (cardDepth < i) {
                                    cardDepth = i;
                                    if (bestActionIndex >= 5) {
                                        bestAction = fca;
                                        bestActionIndex = 5;
                                    }
                                }

                            }
                        }
                    }
                }*/
                /*else if (fca.actionTypeId == 3 && fcn.getCols()[fca.cardDestination()].num == 0) {
                    if (bestActionIndex > 6 ) {
                        bestAction = fca;
                        bestActionIndex = 6;
                    }
                }*/
            }
            //bestActionIndex = -1;
            return bestAction;
        }
        return null;
    }
}
