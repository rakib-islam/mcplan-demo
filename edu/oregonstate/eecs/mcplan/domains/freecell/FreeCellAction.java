package edu.oregonstate.eecs.mcplan.domains.freecell;


import algs.model.searchtree.IMove;
import algs.model.searchtree.INode;

/**
 * Created by mislam1 on 1/25/16.
 */
public abstract class FreeCellAction implements IMove {
    public int actionTypeId = -1;//1,2,3,4,5
    private int noOfCard;

    @Override
    public boolean execute(INode iNode) {
        return false;
    }

    @Override
    public boolean undo(INode iNode) {
        return false;
    }

    @Override
    public boolean isValid(INode iNode) {
        return false;
    }
    public int getActionTypeId(){
        return this.actionTypeId;
    }

    //how many cards we are going to transfer with this action
    public abstract int cardCount();
    public abstract int cardSource();
    public abstract int cardDestination();
    public abstract int cardSuit();
    public abstract int cardRank();
    public abstract int cardColor();
}
