package edu.oregonstate.eecs.mcplan.domains.freecell;

import algs.model.searchtree.INode;

/**
 * Move a card from a free cell to a foundation.
 * 
 * @author George Heineman
 */
public class FreeToFoundationMove extends FreeCellAction {

	/** card to be removed. */
	short card;
	
	/** suit of this card. */
	short suit;
	
	/** rank of this card. */
	short rank;
	
	public FreeToFoundationMove (short card) {
		this.card = card;
		this.actionTypeId = 2;
	}
	
	/**
	 * Execute the move on the given board state.
	 * 
	 * @param n   state on which to execute the move.
	 */
	public boolean execute(INode n) {
		if (!isValid(n)) {
			return false;
		}
		
		FreeCellNode state = (FreeCellNode) n;

		state.removeFree(card);
		state.insertFoundation(card);
		
		return true;
	}

	/**
	 * Determine if move is valid for the given state.
	 * 
	 * @param n
	 */
	public boolean isValid(INode n) {
		FreeCellNode state = (FreeCellNode) n;
		
		if (card == 0) { return false; }
		
		suit = (short)((card-1)%4);     // subtract 1 since '0' is invalid card.
		rank = (short)(1 + ((card-1)>>2));  // rank extracted this way.
		
		return (state.foundationEncoding[suit] + 1 == rank);
	}

	/** 
	 * Assume move had been valid, so the undo is a straightforward swap.

	 * @param n    game state whose move is to be undone.  
	 */
	public boolean undo(INode n) {
		FreeCellNode state = (FreeCellNode) n;
		state.removeFoundation(suit);
		state.insertFree(card);
		return true;
	}

	@Override
	public int cardCount() {
		return 1;
	}

	@Override
	public int cardSource() {
		return -1;
	}

	@Override
	public int cardDestination() {
		return 0;
	}

	@Override
	public int cardSuit() {
		return suit;
	}

	@Override
	public int cardRank() {
		return rank;
	}

	@Override
	public int cardColor() {
		return 0;
	}

	public int getActionTypeId(){
		return this.actionTypeId;
	}
	/** Reasonable implementation. */
	public String toString () {
		return "move " + FreeCellNode.out(card) + " from free cell to foundation.";
	}
}
