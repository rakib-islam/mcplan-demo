package edu.oregonstate.eecs.mcplan.domains.freecell;

import algs.model.list.DoubleLinkedList;
import algs.model.searchtree.IMove;
import algs.model.searchtree.IScore;
import edu.oregonstate.eecs.mcplan.Simulator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by mislam1 on 1/25/16.
 */
public class FreeCellSimulator extends Simulator<FreeCellNode,IMove> {
    private static final int NUMBER_OF_AGENTS = 1;
    private static final String DEAL_FILE = "freecellgames";
    private int gameNumber;
    private static IScore scoringFunction;
    private static IScore scoringFunction1;

    public FreeCellSimulator() {
        int gameNumber = (int)(Math.random() % 32000) + 1;
        this.gameNumber = gameNumber;
        this.scoringFunction = new BoardScorer();
        this.scoringFunction1 = new FreeCellEvaluator();
        setInitialState();
        //todo: compute the actionslist
        //todo: computer the reward for each action

    }
    //if we are given a game number
    public FreeCellSimulator(int gameNumber) {
        this.scoringFunction = new BoardScorer();
        this.scoringFunction1 = new FreeCellEvaluator();
        if (gameNumber > 0 && gameNumber <= 32000) {
            this.gameNumber = gameNumber;
            setInitialState();
        }
    }

    public FreeCellSimulator(FreeCellNode state, List<IMove> legalActions, int[] rewards) {
        state_ = state;
        legalActions_ = new ArrayList<IMove>();
        legalActions_ = legalActions;
        rewards_ = rewards;
    }

    @Override
    public Simulator<FreeCellNode, IMove> copy() {
        return new FreeCellSimulator(state_, legalActions_, rewards_);
    }

    @Override
    public void setInitialState() {
        try {
            state_ = initialize(new File(DEAL_FILE), this.gameNumber);
        } catch (IOException e) {
            e.printStackTrace();
        }
        computeLegalActions();
        computeReward();
    }

    private void computeReward() {
        rewards_ = new int[NUMBER_OF_AGENTS];
        /*for (IMove iMove : listOfActions) {
            System.out.println(iMove.toString());
        }*/
        if (null == scoringFunction) {

        }
        scoringFunction.score(state_);
        if (legalActions_.isEmpty()) {
            if (state_.score == -30 )
                rewards_[0] = 100000;
            else rewards_[0] = 30;
        }
        else {
            rewards_[0] = 160 - state_.score;
        }
    }
    private void computeLegalActions() {
        legalActions_ = new ArrayList<IMove>();
        DoubleLinkedList<IMove> listOfActions  = state_.validMoves();
        for (IMove action : listOfActions) {
            legalActions_.add(action);
        }

        //System.out.println("current reward: Board Scorer" + scoringFunction.eval(state_) + " , FreeCellEvaluator for Astar: " + scoringFunction1.eval(state_));

    }

    public static final FreeCellNode initialize(File dealFile, int dealNumber) throws IOException {
        java.util.Scanner sc = new java.util.Scanner(dealFile);
        for (int i = 0; i < 32000; i++) {
            String line = sc.nextLine();
            if (i < dealNumber) {
                continue;
            }

            StringTokenizer st = new StringTokenizer(line, "., ");
            // get task no.
            int val = Integer.valueOf(st.nextToken()); // bypass number
            assert (val == i);

            // construct deal "shuffle" sequence
            int[] deals = new int[52];
            int idx = 0;
            while (st.hasMoreTokens()) {
                deals[idx++] = Integer.valueOf(st.nextToken());
            }

            // prepare the initial board.
            return initialize(deals);
        }

        // not found!
        return null;
    }

    static FreeCellNode initialize(int[] deals) {
        // Gives cards AC - KC, AD - KD, AH - KH, AS - KS
        short[] cards = new short[52];
        for (short i = 0; i < 52; i++) {
            cards[i] = (short)(1+i);
        }

        // now process the deal number
        short[][] dealt = new short[9][9];
        for (short i = 0; i < 9; i++) {
            for (short j = 0; j < 9; j++) {
                dealt[i][j] = 0;
            }
        }

        int wLeft = 52;
        for (int i = 0; i < deals.length; i++) {
            int j = deals[i];
            dealt[(i%8)+1][i/8] = cards[j];
            cards[j] = cards[--wLeft];
        }


        Column[] cols = new Column[8];
        for (int c = 0; c < 8; c++) {
            cols[c] = new Column();
        }


        for (int r = 0; r <= 6; r++) {
            for (int c = 1; c <= 8; c++) {
                if (dealt[c][r] != 0) {
                    cols[c-1].add(dealt[c][r]);
                }
            }
        }

        return new FreeCellNode(new short[]{0,0,0,0}, new short[]{0,0,0,0}, cols);
    }

    @Override
    public void setState(FreeCellNode state) {
        state_ = state;
        computeLegalActions();
        computeReward();
    }

    @Override
    public void setState(FreeCellNode state, List<IMove> legalActions) {
        state_ = state;
        legalActions_ = legalActions;
        computeReward();
    }

    @Override
    public void takeAction(IMove action) {
        //System.out.println(action);
        //System.out.println(state_);
        FreeCellNode nextNode = state_.copy();
        if (nextNode == null) {
            System.out.printf("hi");
        }
        action.execute(nextNode);
        //System.out.println(nextNode);
        state_ = new FreeCellNode(nextNode.freeEncoding, nextNode.foundationEncoding, nextNode.cols);
        computeLegalActions();
        computeReward();


    }

    @Override
    public int getNumberOfAgents() {
        return NUMBER_OF_AGENTS;
    }

    @Override
    public double[] getFeatureVector(IMove action) {
        return new double[0];
    }

    /*@Override
    public boolean isTerminalState() {
        if (legalActions_.size() == 0)
            return true;
        else {
            int foundationFullCount = 0;
            int foundationAlmostFullCount = 0;
            for (int i = 0; i < state_.foundationEncoding.length; ++i) {
                if (state_.foundationEncoding[i] == 13) {foundationFullCount++;}
                else if (state_.foundationEncoding[i] == 12) {foundationAlmostFullCount++;}
            }
            if (foundationFullCount == 3 && foundationAlmostFullCount == 1) {
                return true;
            }
            else if (foundationFullCount == 2 && foundationAlmostFullCount == 2) {
                return true;
            }
            else if (foundationFullCount == 1 && foundationAlmostFullCount == 3) {
                return true;
            }
            else if (foundationFullCount == 0 && foundationAlmostFullCount == 4) {
                return true;
            }
        }
        return false;

    }*/
}
