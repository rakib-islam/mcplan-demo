package edu.oregonstate.eecs.mcplan.domains.freecell;

import algs.debug.IGraphEntity;
import algs.model.searchtree.IMove;
import algs.model.searchtree.INode;

import java.util.List;

/**
 * Created by mislam1 on 1/30/16.
 */
public interface FreeCellINode extends IGraphEntity {

    List<IMove> validMoves();

    void score(int var1);

    int score();

    FreeCellINode copy();

    boolean equivalent(INode var1);

    Object key();

    Object storedData(Object var1);

    Object storedData();
}