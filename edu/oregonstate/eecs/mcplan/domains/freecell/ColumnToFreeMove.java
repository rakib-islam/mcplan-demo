package edu.oregonstate.eecs.mcplan.domains.freecell;

import algs.model.searchtree.INode;

/**
 * Move a card from a column to a free cell.
 * 
 * @author George Heineman
 */
public class ColumnToFreeMove extends FreeCellAction {

	/** nth column from which card comes. */
	public final int nth;
	
	/** card to be removed. */
	short card;
	
	
	public ColumnToFreeMove(int nth) {
		this.nth = nth;
		this.actionTypeId = 5;
	}
	
	/**
	 * Execute the move on the given board state.
	 * 
	 * @param n   state on which to execute the move.
	 */
	public boolean execute(INode n) {
		if (!isValid(n)) {
			return false;
		}
		
		FreeCellNode state = (FreeCellNode) n;

		//state.detach(nth);
		Column col = state.cols[nth];
		
		col.remove();
		if (col.num == 0) { 
			state.sortMap(); 
		}
		state.insertFree(card);
		
		return true;
	}

	/**
	 * Determine if move is valid for the given state.
	 * 
	 * @param n
	 */
	public boolean isValid(INode n) {
		FreeCellNode state = (FreeCellNode) n;
		
		Column col = state.cols[nth];
		if (col.num == 0) return false;
		
		card = (short) col.cards[col.num-1];
		return state.availableFree();
	}

	/** 
	 * Assume move had been valid, so the undo is a straightforward swap.

	 * @param n    game state whose move is to be undone.  
	 */
	public boolean undo(INode n) {
		FreeCellNode state = (FreeCellNode) n;
		
		state.removeFree(card);
		
		Column col = state.cols[nth];
		col.add(card);
		return true;
	}
	@Override
	public int cardCount() {
		return 1;
	}

	@Override
	public int cardSource() {
		return nth;
	}

	@Override
	public int cardDestination() {
		return -1;
	}

	@Override
	public int cardSuit() {

		int suit = ((card-1)%4);       // subtract 1 since '0' is invalid card.
		return suit;

	}

	@Override
	public int cardRank() {
		int rank = 1 + ((card-1)>>2);  // rank extracted this way.return 0;
		return rank;
	}

	@Override
	public int cardColor() {
		return 0;
	}

	public int getActionTypeId(){
		return this.actionTypeId;
	}
	/** Reasonable implementation. */
	public String toString () {
		return "move " + FreeCellNode.out(card) + " from column " + nth + " to freecell.";
	}
}
