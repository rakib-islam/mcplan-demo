package edu.oregonstate.eecs.mcplan.agents;

import edu.oregonstate.eecs.mcplan.Agent;
import edu.oregonstate.eecs.mcplan.Simulator;
import edu.oregonstate.eecs.mcplan.State;
import edu.oregonstate.eecs.mcplan.domains.galcon.GalconAction;
import edu.oregonstate.eecs.mcplan.domains.galcon.GalconLaunchAction;
import edu.oregonstate.eecs.mcplan.domains.galcon.GalconState;
import edu.oregonstate.eecs.mcplan.domains.galcon.Planet;

import java.util.List;

public class CheatGalconAgent extends Agent {

	
	public CheatGalconAgent() {
		name_ = "Cheating Galcon Agent";
	}
	
	@Override
	public <S extends State, A> A selectAction(S state,
			Simulator<S, A> iSimulator) {
			return (A)cheatStrategy((GalconState)state);
	}

	@Override
	public <S extends State, A> List<A> selectActions(S state, Simulator<S, A> iSimulator) {
		return null;
	}

	private GalconAction cheatStrategy(GalconState state) {
		List<Planet> enemyPlanets = AgentHelper.getEnemyPlanets(state);
		Planet source = AgentHelper.getLargestPlanet(enemyPlanets);
		if (source == null) {
			return AgentHelper.getMatchingNothingAction(state);
		}
		Planet dest = AgentHelper.getFarthestUntargetedPlanet(state, source);
		if (dest == null) {
			return AgentHelper.getMatchingNothingAction(state);
		}
		return new GalconLaunchAction(AgentHelper.getMyAgentId(state), 
				source.getPlanetID(), dest.getPlanetID(), 
				GalconLaunchAction.LaunchSize.LARGE);
	}
}
